class CreateEmergencies < ActiveRecord::Migration
  def change
    create_table :emergencies do |t|
      t.string :police
      t.string :fire
      t.string :ambulance
      t.string :hospital
      t.string :insurance
      t.string :telephone
      t.string :gas_heat
      t.string :electric
      t.string :newspaper
      t.string :radio
      t.string :tvchannel

      t.timestamps
    end
  end
end
