# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140128135310) do

  create_table "biachecklists", :force => true do |t|
    t.string   "Divison_Department"
    t.string   "activity"
    t.string   "financial_loss"
    t.string   "Reputation_image"
    t.string   "Stakeholder_impact"
    t.string   "Regulatory_Statutory"
    t.string   "overall_rating"
    t.string   "comments"
    t.string   "mao"
    t.string   "person_interviewed"
    t.string   "facality"
    t.string   "date"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "businessunits", :force => true do |t|
    t.string   "b_name"
    t.string   "b_site"
    t.string   "b_area"
    t.string   "b_city"
    t.string   "b_state"
    t.string   "b_country"
    t.string   "time_zone"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "crises", :force => true do |t|
    t.string   "crisis_id"
    t.string   "summary"
    t.string   "Datetime_occured"
    t.string   "Details"
    t.string   "crisis_status"
    t.string   "crisis_Category"
    t.string   "crisis_test"
    t.string   "crisis_type"
    t.string   "crisis_severity"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "dashboards", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "documents", :force => true do |t|
    t.string   "title"
    t.string   "location"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "emergencies", :force => true do |t|
    t.string   "police"
    t.string   "fire"
    t.string   "ambulance"
    t.string   "hospital"
    t.string   "insurance"
    t.string   "telephone"
    t.string   "gas_heat"
    t.string   "electric"
    t.string   "newspaper"
    t.string   "radio"
    t.string   "tvchannel"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "plans", :force => true do |t|
    t.string   "plan_title"
    t.string   "plan_objective"
    t.string   "auditmetric"
    t.string   "sucesscriteria"
    t.string   "launchcriteria"
    t.string   "sponsor"
    t.string   "responsible"
    t.string   "recoverableresponse"
    t.string   "planstatus"
    t.string   "recoverableplantype"
    t.string   "losstype"
    t.string   "emergencyresponse"
    t.string   "review"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "policies", :force => true do |t|
    t.string   "p_name"
    t.string   "p_code"
    t.string   "p_scope"
    t.string   "p_objective"
    t.string   "p_description"
    t.integer  "user_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.string   "attach_file_name"
    t.string   "attach_content_type"
    t.integer  "attach_file_size"
    t.datetime "attach_updated_at"
  end

  create_table "risks", :force => true do |t|
    t.string   "risk_id"
    t.string   "risk"
    t.string   "consequence_current"
    t.string   "consequence_target"
    t.string   "likelihood_current"
    t.string   "likelihood_target"
    t.string   "risk_level"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "teams", :force => true do |t|
    t.string   "post"
    t.string   "name"
    t.string   "dept"
    t.string   "destination"
    t.string   "officeno"
    t.string   "mobile"
    t.string   "email"
    t.string   "responsible"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tests", :force => true do |t|
    t.string   "training_name"
    t.string   "training_date"
    t.string   "nexttraining_date"
    t.string   "trainingplan_feedback"
    t.string   "test_date"
    t.string   "test_type"
    t.string   "test_responsible"
    t.string   "test_feedback"
    t.string   "test_status"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
